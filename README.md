# N2942 XPRESS

## A combined final project for two courses : WEB Programming (Frontend) and WEB Server Programming (Backend)

* Author: Martin Aedma, N2942
* Subject: Simple Online ticket booking system for imaginary Railway company> Frontend=ReactJS > Backend=Laravel/PHP > Database=SQL
* Summer 2020

## Contents:
* [N2942 Xpress WEBapp link](http://64.225.98.184/) (Was hosted on Digital Ocean Droplet cloud server : Droplet no longer operational)
* [Report](Docs/Report.md)
* [Code - Frontend = ReactJS](https://student.labranet.jamk.fi/~N2942/webfinal/frontend/)
* [Code - Backend = Laravel/PHP](https://student.labranet.jamk.fi/~N2942/webfinal/backend/)
* [Video](https://youtu.be/DAkRtWAfDwQ)
* [ZIP archive](Docs/n2942-xpress.zip)

Report is written in Finnish, as was required in the courses
