# N2942 XPRESS - Harjoitustyö WEB Palvelinohjelmointi sekä WEB Ohjelmointi kursseille

* N2942 XPRESS - Junalippujen varaussysteemi > Frontend=ReactJS > Backend=PHP/Laravel
* N2942 Martin Aedma
* Kesä 2020

## Sisällysluettelo 

1. [Johdanto](#johdanto)
2. [Tekijästä](#tekij%C3%A4st%C3%A4)
3. [Sovelluksen yleiskuvaus](#sovelluksen-yleiskuvaus)
4. [Käyttäjäroolit](#k%C3%A4ytt%C3%A4j%C3%A4roolit)
5. [Käyttöympäristö ja käytetyt teknologiat](#k%C3%A4ytt%C3%B6ymp%C3%A4rist%C3%B6-ja-k%C3%A4ytetyt-teknologiat)
6. [Toiminnalliset vaatimukset](#toiminnalliset-vaatimukset)
7. [Käyttötapaukset](#k%C3%A4ytt%C3%B6tapaukset)
8. [Käsitemalli](#k%C3%A4sitemalli)
9. [Frontend toteutus](#frontend-toteutus)
10. [Backend toteutus](#backend-toteutus)
11. [Luokkamalli](#luokkamalli)
12. [Työnjako](#ty%C3%B6njako)


## Johdanto

Tämä projekti on opiskelijan N2942 harjoitustyö opintojaksoille - WEB Palvelinohjelmointi sekä WEB Ohjelmointi, kesä 2020.

## Tekijästä

N2942 on JAMK 1. vuoden ICT opiskelija, suuntautunut ohjelmistotekniikkaan. Koko ohjelman tekee N2942 (Martin Aedma).

## Sovelluksen yleiskuvaus

Sovelluksen löydät täältä - [N2942 XPRESS WEBAPP](http://64.225.98.184/)

Tehtävänä on todeuttaa kuvitelmalliselle, juna-kuljetusta tarjoavalle yritykselle N2942 XPRESS online lippu-varaussysteemi.
Asiakkaat pystyvät tekemään käyttäjätilin ja varamaan matkalippuja.

## Käyttöympäristö ja käytetyt teknologiat

*  Toteutus JavaScript/ReactJS/JSX ja PHP/Laravel ohjelmointikieliä käyttäen 
*  Käyttöliittymä, frontend puoli ReactJS käyttäen sisältää WEB Ohjelmointi osion harjoitustyöstä
*  Palvelimen ohjelmointi PHP/Laravelilla todeutettuna sisältää WEB Palvelinohjelmointi osion harjoitustyöstä
*  Käytetty Reactille kirjoitettuja julkisia komponentteja: react-datepicker, google-map-react
*  Palvelimen RESTFUL API toimintaa suojattu Laravel Passportia käyttäen
*  Tietokantana MySQL relaatiotietokanta
*  Palvelimena toimii Digital Oceanin OCEAN Droplet palvelin Ubuntu käyttöjärjestelmällä

## Toiminnalliset vaatimukset

| Vaatimus | Kuvaus |							
|:-:|:-:|
| Käyttäjätili | Jokainen asiakas pystyy tekemään salasanalla suojatun käyttäjätilin | 
| Lippujen varaus | Käyttäjä pystyy päivämäärän, lähtö -ja päätepysäkin perusteella varamaan matkalippuja |
| Varausten määrä | Käyttäjä pystyy kerralla vaarmaan max 5 lippua | 
| Poikkeusten käsittely | Mahdolliset poikkeukset pitää olla käsitelty jotta asiakas pystyisi varamaan lipun ja ohjelma ei kaatu | 
| Lippujen tarkistus  | Käyttäjä pystyy tarkistamaan ostamansa lippuja kun hän on kirjautunut tilille |
| Google Map  | Käyttäjälle on tarjoilla Google Map kartta missä näkyy juna reitit |

## Käyttötapaukset

```plantuml
:User: --> (Create Account)
(Create Account) --> (Name)
(Create Account) --> (Email)
(Create Account) --> (Password)

(Name) --> (Database)
(Email) --> (Database)
(Password) --> (Database)
```

```plantuml
:User: --> (Login)
(Login) --> (Email)
(Login) --> (Password)
(Email) --> (Database)
(Password) --> (Database)
```

```plantuml
:User: --> (Book Ticket)
(Book Ticket) --> (Date)
(Book Ticket) --> (From)
(Book Ticket) --> (To)
(Book Ticket) --> (No. Passengers)
(Date) --> (Validation)
(From) --> (Validation)
(To) --> (Validation)
(No. Passengers) --> (Validation)
(Validation) --> (Database)
```

```plantuml
:User: --> (Check ticket)
(Check ticket) --> (Database)
(Database) --> (Check ticket)
```

## Käsitemalli

### Frontend

![](frontend.png)

### Backend

![](backend.png)

## Frontend toteutus

Ohjelma sijaitse:
[N2942 XPRESS](http://64.225.98.184/)

Kaikkien React komponentien lähdekoodit löytyvät tiedostoista!

### Login view

![](loginview.png)

Login näkymä on toteudettu yhdellä React komponentilla. Reactin AJAX kutsuille on käytetty perus fetch toiminnallisuutta. Login komponentin form lähettää käyttäjän 
sisäistämät tiedot POST metodilla palvelimen julkiselle WEB 'login' reitille missä autentikoitaan käyttäjä. Sama aikaa tehdään myös fetch pyyntö API/core POST reitille mistä 
haetaan käyttäjälle bearer token API pyyntöjä varten.

### Register view

![](registerview.png)

Register näkymä on todeutettu myös yhdellä React komponentilla. Selaimen puolella tarkistetaan että käyttäjä ei voi lähettä tyhjiä kenttiä sekä se, että password ja confirm täytyy 
matchaa (tämä myös tarkistetaan lisäksi vielä palvelimen puolella). Tiedot lähetetään 'api/auth/signup' reitille missä validoitaan sisäisteyt tiedot ja lisätään käyttäjä tietokantaan.
Onnistunut lisäys ohjaa käyttäjän takaisin 'login' näkymälle ja ilmoittaa että käyttäjä on tehty.

### Core view

![](coreview.png)

Core view on päänäkymä. Kirjautunut käyttäjä pystyy täältä katsomaan Google map kauttaa junareittejä, varamaan lippuja ja myös katsomaan ostettuja lippuja.
Core on toteudettu neljällä React komponentilla:

1. Map.js 
    Google mapia tarjoava komponentti, tässä käytetään google-map-react julkisesti tarjoilla oleva komponenttia. Reittien pysäkit ovat kovakoodattu tähän komponenttin - hienompi
    ratkaisu olisi lisätä tietokantaan stops tauluun jokaiselle pysäkille lat ja lng ominaisuudet ja hakea ne sieltä. Mut jätin nyt näin. Polyline viivalla piirretään kartalle 
    reitit.
2. RouteInfo.js
    Pieni komponentti, näyttää tekstillä kirjoitettuna reittien tiedot.
3. Dates.js
    Tämä on isoin React komponentti ja hoitaa käyttäjän sisäistämät tiedot välittämällä ne palvelimelle. Päivämäärän valintaan käytetään julkista React komponenttia React-datepicker.
    Klikkamalla Date kentälle aukeaa Datepicker:in kalenteri. Mistä käyttäjä saa mukavasti valita sopivan päivämäärän. Tämän kentän data muoto muokataan palvemille sopivaksi ennen 
    lähetystä.
    To ja From kentät toimivat autocomplete toiminnalla. Käyttäjän sisäistämät kirjamiet (kentän muutokset laukevat onChange tapahtuman) lähetetään fetch:illä palvelimelle.
    Pysäkit, mitkä alkavat niillä kirjaimilla tulostetaan listaan. Tässä käytetään datalist elementtia.
    Selaimen puolella tarkistetaan kenttien arvot jotta ne ei olisi tyhjät, muuten tarkaistus tapahtuu palvelimen puolella.
    Jos tietoissa on jotain väärin niin palvelimelta tulee ilmoitus ja se tulostetaan käyttäjälle.
    Jos lipun varaus onnistu niin siitä tule ilmoitus ja lippua voi katsoa My tickets napista.
    Ajax pyyntöihin tarvittava bearer token on tallennettu komponentin tilamuuttujan.
4. UserTickets.js
    Tämä komponentti tulosta käyttäjälle hänen varattuja lippuja. Komponentin DidMount kohtassa tehdään Ajax pyyntö millä haetaan tämän käyttäjän kaikki varatut liput. Oletuksena 
    lippuja ei heti näytetä käyttäjälle. Mikäli käyttäjä paina My tickets nappia niin sillä lippuja pääse näkee tai piilottaa.
    Tähäm komponenttiin on myös lisätty Logout nappi ja toiminta millä käyttäjä pääse kirjautumaan ulos sovelluksesta ja palautetaan taas login näkymä.

### Yleisesti frontend ohjelmoinnista

React webappin ohjelmointiin Laravel projektin yhteydessä käytettiin NodeJS:iä ja npm:iä. Ohjelmoinnin aikana käytin npm run watch komentoa mikä seuraa muutoksia webapin tiedostoissa 
ja rakentaa automaattisesti uuden version ohjelmasta. Kaikki sovelluksessa käytettävät React komponentit on lisätty sovellukseen app.js tiedoston kauttaa, esm < require('./components/UserTickets'); >
Sovelluksen nettisivuina käytetään Laravel *.blade.php sivuja. Jokaiseen semmosen sivuun on lisätty 

> <script src="{{mix('js/app.js')}}" ></script>

tägi mikä lataa Reactin.
Itellä tuntui homma sujuvan, harkkojen React osio antoi hyvä yleisen käsityksen Reactista, komponentteista sekä miten komponentteja rakentaan ja käytetään.

## Backend toteutus

### RESTFUL API

Backend puolen isoin tehtävä oli tarjota RESTFUL toiminnan kauttaa tietoja tietokannasta (tai tallentaa tietoja tietokantaan). Tässä toteutuksessa itseasiassa ei ole käytetty 
PUT (tietojen muokkausta) tai DELETE (tietojen poisto) toiminta. Isoin osa backend puolen ohjelmointia oli tietokantaan lähetettävien kyselyiten koodausta sekä käyttäjältä tulevan 
tieton validointia.

### API suojattu Laravel Passportilla

Laravel passport tarjoa Bearer Token systeemin API kyselyiden suojaamiseksi. Jokainen käyttäjä pitää lähettää pyynnön yhteydessä, Authorization header:in missä on Bearer Token sisällä.
Tämä toimiva token myönnetään käyttäjälle kun hän kirjautu sisään.

### API suojatut / authenticated reitit

Ylhäällä oleva käsitemalli selostaa palvelimella käytetyt reitit, paitsi API suojatut reitit. Seuraavaksi niistä tarkemmin:

Middleware:ina käytetään auth:api

> Route::middleware('auth:api')->get('/user', function (Request $request) {
>     return $request->user();
> });

Ja TripController:in konsturktorissa

> public function __construct()
>     {
>         $this->middleware('auth:api');
>     }

API suojatut reitit

* Route::get('stops', 'TripController@getStops') - pysäkkien autocomplete hakuun kun käyttäjä frontendissä sisäistä To ja From kenttiin aakkosia.
* Route::get('trip', 'TripController@getUserTrips') - käyttäjän varatut liput haetaan frontendiin tätä kautta (My tickets)
* Route::post('trip', 'TripController@addTrip') - uusi lippu varaus, lähetetään tietoja
* Route::get('validateTicket', 'TripController@checkTicket') - valitoitaan käyttäjän lähettämät tiedot lippu varausta varten.

### Laravel ORM Eloquent query builder esimerkki

Iso osa backend ohjelmoinnista oli tietokantakyselyiden rakentamista. TripController sisältää käyttäjän kannalta kaikki tärkeät kyselyt. Käytin query builderia niin kuten 
harjoitustehtävissä tehtiin, tässä yksi esimerkki liitoskyselystä millä haetaan käyttäjän varattuja lippuja ja sen lipun tarkemmat tiedot.

Esimerkki:

>   public function getUserTrips() {
> 
>         $id = auth('api')->user()->id;
>         $tripInfo = json_encode(
>             DB::table('trips')
>             ->select('trips.id as TicketNumber',
>                 'users.name as User',
>                 'users.email as Email',
>                 'trainroutes.id as RouteNumber',
>                 'trainroutes.description as RouteInfo',
>                 'trains.name as TrainName',             
>                 'trips.date as Date',
>                 'stops1.name as From',
>                 'routestops1.departure as Departure',
>                 'stops2.name as To',                
>                 'routestops2.arrival as Arrival',
>                 'trips.created_at as Purchased'
>             )
>             ->join('users', 'user_id','=','users.id')
>             ->join('trains', 'train_id','=','trains.id')
>             ->join('trainroutes','train_route_id','=','trainroutes.id')
>             ->join('stops as stops1','stop_from','=','stops1.id')
>             ->join('stops as stops2','stop_to','=','stops2.id')
>             ->join('routestops as routestops1','routestops1.stop_id','=','stops1.id')
>             ->join('routestops as routestops2','routestops2.stop_id','=','stops2.id')
>             ->where('user_id','=', $id)
>             ->orderBy('Purchased','desc')
>             ->get()
>         );
>         
>         return $tripInfo;
>     }

Näin varmistetaan että käsitellaan ainoastaan tämän, sovellukseen kirjautunut käyttäjän tietoja
> $id = auth('api')->user()->id;
 
### Rexeg validointi

Laravel Validator:ia käytettiin regex kanssa käyttäjän lähettämien tietojen validointiin, esimerkki:

>       $validator = Validator::make($request->all(), [
>
>             'date' => ['required','regex:/^(2020-08-([0-2][0-9]|30))$/'],
>
>             'train_id'=> ['required','regex:/^(1|2)$/'],
>
>             'train_route_id'=> ['required', 'regex:/^(1|2)$/'],
>
>             'stop_from'=> ['required','regex:/^[1-8]$/'],
>
>             'stop_to'=> ['required','regex:/^[1-8]$/'],
>
>         ]);
>
>         if ($validator->fails()) {
>
>             return ("ERROR ADDING TRIP");
>
>         }


### JSON

Kaikki frontend lähetetyt kyselyt sekä palvelimelta lähetetyt vastaukset on JSON muodossa, käytetään json_encode ja json_decode metodeja.

### Yleisesti backend ohjelmoinnista

Ocean dropletin käyttönoton tein jo harjoitustehtävissä niin tämä ei ollut vaikea tehtävä. Laravel tarjoa paljon hyviä ja yksinkertaisia metodeja millä toteuttaa erilaisia tehtäviä palvelimen puolella.
Harkkojen Laravel tehtävät antoivat hyvä käsityksen tarvittavasta tietosta, niitä tuli käytetty. Laravelin Passport systeemin kanssa oli pikkasen hankaluksia alussa mutta sitten 
ymmärsin asian paremmin ja alkoi toimii. Muutaman kerran kävin stackoverflow:ssa myös kysymässä apua. Mutta yleisesti homma tuntui sujuvan.
Backend reittien testaamisen kehittämisen aikana otin käyttön Postman ohjelman, oli tosi kätevä ja helppo kokeilla reitteja.

## Tietokanta

Koko sovelluksen idea oikein alkoi tietokannasta. Keväällä suoritin Tietokannat opintojakson mihin tein tämän junalippujen varaussysteemin tietokanta pohjan. Silloin oli vapaaehdoinen 
että tekisi tietokannalle web pohjaisen käyttöliittymän. En oikein käsittänyt vielä miten sen tekisin mutta nyt WEB ohjelmoinnin sekä palvelinohjelmoinnin jälkeen oli hyvä 
harjoitella.

Sovelluksessa käytetyn tietokannan tarkemmat tiedot:

[Vaatimusmäärittely](Vaatimusmäärittely_N2942.pdf)

[Käsitemalli](Käsitemalli_N2942.pdf)

[Raportti](Loppuraportti_N2942.pdf)

Tämä tietokanta muutettiin Laravelin malleiksi (Model) ja tehtiin migraatiot (php artisan migrate).

## Lisätietoja ohjelmasta

Sovelluksessa on rajoitetut junareitit ja miten junat liikkuu - en ruvennut sisäistä liika tietoja ja mahdollisuuksia.

* Sovelluksessa on tarjoilla 2 junareittia
* Junat kulkevat vain lähtöasemasta kohti päätepysäkkiä, eli ei voi varata lippua mikä kulkee takasipäin - sovellus ilmoittaa jos yrittää.
* Junat kulkevat vain aikavälillä 01.08.2020 - 31.08.2020 jos yrittää varata sen aikavälin ulkopuolella sovellus ilmoitta virhen.
* 1 juna per reitti per päivä. Eli ei ole kelloaikoijen mahdollisuutta. 
* Sovelluksen tämä POC-versio on testattu toimivan vain Chrome selaimella.
* Iphone puhelimella (sekä Safari että Chrome:lla) tai MAC koneella Safari selaimella esintyy ongelma missä ei näkyy "Logout" ja "My Tickets" nappeja.

## Luokkamalli

### Frontend 

Käyttöliittymä on jaettu kolmen sivun - Login / Register / Core
Login ja Register todeutettu kumpikin yhdellä React komponentilla, Core neljällä.

### Backend

Sovellus jaettu WEB ja API reitteihin / kontrolloreihin.

WEB reittien ohjelmointi todeutettu: PublicController ja HomeController. Nämä ovat yksinkertaista käyttäjän ohjaamista oikein paikkoihin.
API reittien ohjelmointi todeutettu:
* TripController - sisältää isoimmain osan koodista ja toimiii lippujen tietojen hakuun sekä varauksen tarvittavasta toiminnasta.
* AuthController - tässä hoitetaan tarvittava koodi jotta käyttäjät pystyvät luomaan tilin (signup), kirjautumaan sisään JA generoimaan Bearer Tokenin (login), kirjautumaan ulos (logout).

## Työnjako

Martin Aedma (N2942) toteutti koko projektin.

## Arvosana ehdotus

Itsestäni tuntuu että suoritin hyvin molemmat opintojaksot, käytin harjoitustyössä opintojaksoilla opetettuja tekniikoita mutta myös uusiakin.
Sain ohjelman toimimaan halutulla tavalla, sitä voisi kyllä vielä täytentää mutta luulen että harjoitustyöksi tein sen riittävän pitkälle. Pystyin soveltamaan molemmat opintjaksot
harjoitustyössä ja kokeilemaan ns "fullstack" osaamista. Tunteja en laskennut mutta tein harjoitustyötä koko heinäkuun ajan, yleisesti vähintään 2-3 tuntia päivässä.
Ite tähtäisin vitosta opintojaksolta ja luulen että tein sen verran töitäkin, eli ehdotan vitosta :)